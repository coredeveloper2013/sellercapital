// Importing Node packages required for schema
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//= ===============================
// User Schema
//= ===============================
const SellerConfigSchema = new Schema({
        SellerId: { type: String, required: true },
        tokenConfig: {type: Object}
    },
    {
        timestamps: true
    });


module.exports = mongoose.model('SellerConfig', SellerConfigSchema);

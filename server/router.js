const AuthenticationController = require('./controllers/authentication');
const OrderController = require('./controllers/order');
const FeedbackController = require('./controllers/feedback');
const MailController = require('./controllers/mail');
const TemplateController = require('./controllers/template');
const CampaignController = require('./controllers/campaign');
const PageController = require('./controllers/page');
const CronJobCustomController = require('./controllers/cronCustom');
const ProductController = require('./controllers/product');
const DemoController = require('./controllers/demo');
const UserController = require('./controllers/user');
const ChatController = require('./controllers/chat');
const CommunicationController = require('./controllers/communication');
const StripeController = require('./controllers/stripe');
const express = require('express');
const passport = require('passport');
const ROLE_MEMBER = require('./constants').ROLE_MEMBER;
const ROLE_CLIENT = require('./constants').ROLE_CLIENT;
const ROLE_OWNER = require('./constants').ROLE_OWNER;
const ROLE_ADMIN = require('./constants').ROLE_ADMIN;

const passportService = require('./config/passport');

// Middleware to require login/auth
const requireAuth = passport.authenticate('jwt', {session: false});
const requireLogin = passport.authenticate('local', {session: false});

module.exports = function (app) {
    // Initializing route groups
    const apiRoutes = express.Router(),
        authRoutes = express.Router(),
        userRoutes = express.Router(),
        chatRoutes = express.Router(),
        payRoutes = express.Router(),
        communicationRoutes = express.Router();

    //= ========================
    // Auth Routes
    //= ========================

    // Set auth routes as subgroup/middleware to apiRoutes
    apiRoutes.use('/auth', authRoutes);


    // Registration route
    authRoutes.post('/register', AuthenticationController.register);


    // Login route
    authRoutes.post('/login', requireLogin, AuthenticationController.login);

    // Password reset request route (generate/send token)
    authRoutes.post('/forgot-password', AuthenticationController.forgotPassword);

    // Password reset route (change password using token)
    authRoutes.post('/reset-password/:token', AuthenticationController.verifyToken);

    //= ========================
    // User Routes
    //= ========================

    // Set user routes as a subgroup/middleware to apiRoutes
    apiRoutes.use('/user', userRoutes);

    // View user profile route
    userRoutes.get('/:userId', requireAuth, UserController.viewProfile);

    // Test protected route
    apiRoutes.get('/protected', requireAuth, (req, res) => {
        res.send({content: 'The protected test route is functional!'});
    });

    apiRoutes.get('/admins-only', requireAuth, AuthenticationController.roleAuthorization(ROLE_ADMIN), (req, res) => {
        res.send({content: 'Admin dashboard is working.'});
    });

    //= ========================
    // Chat Routes
    //= ========================

    // Set chat routes as a subgroup/middleware to apiRoutes
    apiRoutes.use('/chat', chatRoutes);

    // View messages to and from authenticated user
    chatRoutes.get('/', requireAuth, ChatController.getConversations);

    // Retrieve single conversation
    chatRoutes.get('/:conversationId', requireAuth, ChatController.getConversation);

    // Send reply in conversation
    chatRoutes.post('/:conversationId', requireAuth, ChatController.sendReply);

    // Start new conversation
    chatRoutes.post('/new/:recipient', requireAuth, ChatController.newConversation);

    //= ========================
    // Payment Routes
    //= ========================
    apiRoutes.use('/pay', payRoutes);

    // Webhook endpoint for Stripe
    payRoutes.post('/webhook-notify', StripeController.webhook);

    // Create customer and subscription
    payRoutes.post('/customer', requireAuth, StripeController.createSubscription);

    // Update customer object and billing information
    payRoutes.put('/customer', requireAuth, StripeController.updateCustomerBillingInfo);

    // Delete subscription from customer
    payRoutes.delete('/subscription', requireAuth, StripeController.deleteSubscription);

    // Upgrade or downgrade subscription
    payRoutes.put('/subscription', requireAuth, StripeController.changeSubscription);

    // Fetch customer information
    payRoutes.get('/customer', requireAuth, StripeController.getCustomer);

    //= ========================
    // Communication Routes
    //= ========================
    apiRoutes.use('/communication', communicationRoutes);

    // Send email from contact form
    communicationRoutes.post('/contact', CommunicationController.sendContactForm);


    /********************************************** Account Activation *****************************************/
    apiRoutes.use('/active_account/:id', PageController.activeAccount);
    /********************************************** Account Activation *****************************************/

    /******************************************** Order Route ***********************************************/
    // Order route
    apiRoutes.get('/orders', OrderController.list);
    // Filter Orders route
    apiRoutes.post('/orders', OrderController.filterList);
    
    // get products
    apiRoutes.get('/products', ProductController.list);
    apiRoutes.post('/products', ProductController.filterList);
    apiRoutes.get('/product_report_request', ProductController.requestReport);
    apiRoutes.get('/save_product_report/:id', ProductController.saveProductReport);

    // test single product scraping
    apiRoutes.get('/single/product', ProductController.singleProduct);
    apiRoutes.get('/delete/products', ProductController.deleteProducts);

    apiRoutes.get('/orders_from_api', OrderController.getOrder);

    // Sending email to the order maker by rasel
    apiRoutes.post('/send_mail_to_orderer', requireAuth, OrderController.sendMailToOrderer);

    /******************************************** End Order Route ***********************************************/

    /******************************************** FeedBack ***********************************************/
    apiRoutes.get('/feedback', FeedbackController.list);
    apiRoutes.post('/feedback', FeedbackController.filterList);
    apiRoutes.get('/feedback_analysis', FeedbackController.getFeedbackAnalysis);


    // Manual Cron Process //
    apiRoutes.get('/request_report', FeedbackController.requestReport);
    apiRoutes.get('/report_list', FeedbackController.getReportList);
    apiRoutes.get('/report_request_list', FeedbackController.getReportRequestList);
    apiRoutes.get('/report/:id', FeedbackController.getReport);


    // Scrapping Data
    apiRoutes.get('/product_feedback/:id', FeedbackController.getProductFeedback);
    apiRoutes.get('/feedback_report_request', FeedbackController.getAllFeedback);

    /******************************************** FeedBack ***********************************************/


    /******************************************** Template ***********************************************/
    // test file upload
    // apiRoutes.post('/logo_upload', TemplateController.templateFileUpload);
    apiRoutes.post('/create_template', requireAuth, TemplateController.createTemplate);
    apiRoutes.put('/modify_template', requireAuth, TemplateController.update);
    apiRoutes.get('/templates', requireAuth, TemplateController.list);
    apiRoutes.get('/preview_template/:id', requireAuth, TemplateController.previewTemplate);
    apiRoutes.delete('/delete_template/:id', requireAuth, TemplateController.delete);
    /******************************************** Template ***********************************************/

    /******************************************** Campaign ***********************************************/
    apiRoutes.post('/create_campaign', requireAuth, CampaignController.createCampaign);
    apiRoutes.post('/modify_campaign', requireAuth, CampaignController.update);
    apiRoutes.get('/campaigns', requireAuth, CampaignController.list);
    apiRoutes.delete('/delete_campaign/:id', requireAuth, CampaignController.delete);
    /******************************************** Campaign *  end ***********************************************/

    /******************************************** Mail ***********************************************/
    apiRoutes.post('/send-test-template-mail', MailController.sendTestMail);
    apiRoutes.post('/resend_mail', MailController.resentWelcomeMail);

    /******************************************** Mail ***********************************************/


    // Cron Custom Way
    apiRoutes.get('/cron_job/get_orders', CronJobCustomController.orders);
    apiRoutes.get('/cron_job/get_order_items', CronJobCustomController.orderItems);


    // Profile update
    apiRoutes.post('/update_profile', UserController.update_profile);

    // Retrieve single conversation
    apiRoutes.get('/order_item/:id', OrderController.getOrderItem);

    // Order route
    apiRoutes.get('/demo/order/config', DemoController.orderConfig);
    apiRoutes.get('/demo/cron', DemoController.cron);

    // Sending schedule newsletter to user based on order status by rasel
    apiRoutes.get('/cron_job/run_template_scheduler', OrderController.sendScheduleLetter);

    // Set url for API group routes
    app.use('/api', apiRoutes);
};

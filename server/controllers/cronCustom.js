const cron = require('node-cron');
const Order = require('../models/order');
const OrderItem = require('../models/orderItem');
const FeedbackRequest = require('../models/feedbackRequest');
const config = require('../config/main');
const async = require("async");
const Cron = require('../models/cron');
const SellerConfig = require('../models/sellerConfig');

const mws_key = config.MWS_KEY || '';
const mws_access = config.MWS_SECRET || '';
const amazonMws = require('amazon-mws')('AKIAIEGT53RIXYQUCTPQ','VdToubCLaeVs+ngo3g7aIGCUzlqsisfCVWnKCga6');
const moment = require('moment');

module.exports = {
    orders: function (req, res) {
        Cron.findOne({ type: 'order'}, (err, foundOrder) => {
            if (err) { console.log(err); return next(err); }

            if (foundOrder && foundOrder.SellerId) {
                SellerConfig.findOne({SellerId: foundOrder.SellerId}, (err, foundSaller)=>{
                    if(err){
                        console.log(err);
                    }
                    if(foundSaller && foundSaller.SellerId){
                        let sellerId = foundSaller.SellerId;
                        let cDate;
                        if(foundSaller.current_date){
                            cDate = new Date(foundSaller.current_date);
                        }else{
                            cDate = new Date(moment().subtract(3, 'years').format());
                        }
                        let searchQuery = {
                            'Version': '2013-09-01',
                            'Action': 'ListOrders',
                            'SellerId': sellerId,
                            'MWSAuthToken': foundSaller.tokenConfig.MWSAuthToken,
                            'MarketplaceId.Id.1': foundSaller.tokenConfig.id1,
                            'LastUpdatedAfter': cDate
                        };
                        if(foundOrder.NextToken){
                            searchQuery.NextToken = foundOrder.NextToken;
                            searchQuery['Action'] = 'ListOrdersByNextToken';
                        }
                        amazonMws.orders.search(searchQuery, function (error, response) {
                            if (error) {
                                console.log('error ', error);
                                return;
                            }
                            response.sellerId = sellerId;
                            formateSaveUpdateOrderData(response, function (respData) {
                                let cuDate = foundSaller.current_date;
                                if(moment(cDate).isBefore(moment())){
                                    cuDate = new Date(moment(cDate).add(1, 'weeks').format());
                                    Cron.findOneAndUpdate(foundSaller.id, {NextToken: response.NextToken}, (err, selData) => {
                                        return res.status(200).json({ messsage: 'done cron data' });
                                        }
                                    )
                                }else{
                                    Cron.findByIdAndRemove(foundSaller.id, (err, fseller) => {
                                        if (err){console.log(err);}
                                        console.log("The crone has been deleted");
                                    });
                                }
                            });
                        });
                    }
                });

            }

        });
    },
    orderItems: function (req, res) {
        Order.find({haveItem: { $exists: false }}, (err, foundOrders) => {
            if (err) { console.log(err); }
            async.each(foundOrders, function (order, callb) {
                let orderId = order.AmazonOrderId;
                let sellerId = 'A1LWZ980X488GK';
                amazonMws.orders.search({
                    'Version': '2013-09-01',
                    'Action': 'ListOrderItems',
                    'SellerId': sellerId,
                    'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8',
                    'AmazonOrderId': orderId,
                }, function (error, response) {
                    if (error) {
                        console.log(error);
                    }
                    if(response && Object.keys(response).length){

                        let itemData = response.OrderItems.OrderItem;
                        let respOrder = {AmazonOrderId: response.AmazonOrderId, asin: itemData.ASIN, qty: itemData.QuantityOrdered,
                            sku: itemData.SellerSKU, title: itemData.Title, itemId: itemData.OrderItemId,
                            price: itemData.ItemPrice && itemData.ItemPrice.Amount? itemData.ItemPrice.Amount : '',
                            currency: itemData.ItemPrice && itemData.ItemPrice.CurrencyCode?itemData.ItemPrice.CurrencyCode: ''};
                        OrderItem.findOne({ AmazonOrderId: response.AmazonOrderId, itemId: respOrder.itemId}, (err, existingItem) => {
                            if (err) { console.log(err);}
                            // If order is exist
                            if (!existingItem) {
                                respOrder.sellerId = sellerId;
                                const orderData = new OrderItem(respOrder);
                                orderData.save((err, savedOrderItem) => {
                                    if (err) { console.log(err); }
                                    Order.findOne({ AmazonOrderId: savedOrderItem.AmazonOrderId}, function (err, findOrderDta) {
                                        if (err){console.log(err);}
                                        if(findOrderDta){
                                            Order.findById(findOrderDta.id, function (err, orderData) {
                                                if (err){console.log(err);}
                                                orderData.haveItem = true;
                                                orderData.orderItem.push(savedOrderItem.id);
                                                orderData.save(function (err, updatedOrder) {
                                                    if (err){console.log(err);}
                                                    callb(updatedOrder);
                                                });
                                            });
                                        }else{
                                            callb();
                                        }
                                    });
                                });
                            }else{
                                callb({ exist: true, item: existingItem });
                            }
                        });
                    }else{
                        callb();
                    }
                });
            }, function (err) {
                if(err){
                    console.log(err);
                    console.log('there is a problem to get order data from API.');
                }else{
                    return res.status(200).json({ messsage: 'done cron data' });
                    // console.log('Item Order Saved.');
                }
            });
        })
    },

    requestForFeedbackReport: function () {
        cron.schedule('*/20 */13 * * *', function () {
            let startDate = moment().subtract(2, 'days').format();
            let endDate = moment().format();
            let sellerId = 'A1LWZ980X488GK';
            amazonMws.reports.search({
                'Version': '2009-01-01',
                'Action': 'RequestReport',
                'SellerId': sellerId,
                'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8',
                'MarketplaceIdList.Id.1': 'ATVPDKIKX0DER',
                'ReportType': '_GET_SELLER_FEEDBACK_DATA_',
                'StartDate': startDate,
                'EndDate': endDate,
            }, function (error, response) {
                if (error) {
                    console.log('error ', error);
                }else{
                    let respData = response.ReportRequestInfo;
                    FeedbackRequest.findOne({ SellerId: 'A1LWZ980X488GK', ReportRequestId: respData.ReportRequestId}, (err, existingId) => {
                        if (err) { return next(err); }

                        if (existingId) {
                            console.log({ message: 'That seller id is already exist.' });
                        }else{
                            const requestReportData = new FeedbackRequest({SellerId: sellerId, ReportType: respData.ReportType,
                                ReportRequestId: respData.ReportRequestId, StartDate: respData.StartDate, EndDate: respData.EndDate});

                            requestReportData.save((err, savedReqData) => {
                                if (err) { console.log(err); console.log(err); }
                                console.log('request report saved')
                            });
                        }
                    });
                }
            });
        }, false);
    },

    findRequestReportList: function () {
        let sellerId = 'A1LWZ980X488GK';
        cron.schedule('*/10 */14 * * *', function () {
            FeedbackRequest.find({ReportId: {$exists: false}}, (err, allRequests) => {
                if(err){ console.log(err); }
                if(allRequests && allRequests.length){
                    let allReqs = allRequests.map(reqs=>reqs.ReportRequestId);
                    amazonMws.reports.search({
                        'Version': '2009-01-01',
                        'Action': 'GetReportList',
                        'SellerId': sellerId,
                        'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8'
                    }, function (error, response) {
                        if (error) {
                            console.log('error ', error);
                        }
                        let respReports = response.ReportInfo || [];
                        async.each(respReports, function (singleRep, callb) {
                            if(allReqs.indexOf(singleRep.ReportRequestId) > -1){
                                FeedbackRequest.findOne({ ReportRequestId: singleRep.ReportRequestId}, function (err, findFDReqData) {
                                    if (err){console.log(err);}
                                    if(findFDReqData){
                                        FeedbackRequest.findById(findFDReqData.id, function (err, reqData) {
                                            if (err){console.log(err);}
                                            reqData.ReportId = singleRep.ReportId;
                                            reqData.save(function (err, updateReqDta) {
                                                if (err){console.log(err);}
                                                callb();
                                            });
                                        });
                                    }else{
                                        callb();
                                    }
                                });
                            }else{
                                callb();
                            }
                        }, function (err) {
                            if(err){
                                console.log(err);
                                console.log('there is a problem to find request report list.');
                            }else{
                                console.log('Feedback request Saved.');
                            }
                        });
                    });
                }
            })
        });
    },

    feedbackDataStore: function () {
        cron.schedule('*/40 */15 * * *', function() {
            FeedbackRequest.findOne({ReportId: {$exists: true}}, (err, existingReport) => {
                if(existingReport){
                    let reportId = existingReport.ReportId;
                    let sellerId = existingReport.SellerId || 'A1LWZ980X488GK';
                    amazonMws.reports.search({
                        'Version': '2009-01-01',
                        'Action': 'GetReport',
                        'SellerId': sellerId,
                        'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8',
                        'ReportId': reportId
                        //'ReportTypeList.Type.1': 'REPORT_TYPE_LIST' //optional
                    }, function (error, response) {
                        if (error) {console.log(error);}
                        let respData = response.data || [];
                        if(respData.length){
                            let feedbackData = respData.map(rd => {
                                return {date: rd['Date'], rating: rd['Rating'], comment: rd['Comments'],
                                    response: rd['Your Response'], orderId: rd['Order ID'], email: rd['Rater Email']};
                            })

                            async.each(feedbackData, function (feedbackDta, callb) {
                                Feedback.findOne({ sellerId: sellerId, orderId: feedbackDta.orderId, email: feedbackDta.email}, (err, foundFeedback) => {
                                    if (err) { console.log(err);}
                                    // If order is exist
                                    if (!foundFeedback) {
                                        const feedbackData = new Feedback(feedbackDta);
                                        feedbackData.save((err, fdData) => {
                                            if (err) { console.log(err); }
                                            Order.findOne({sellerId: sellerId, orderId: feedbackDta.orderId}, (err, foundOrder) => {
                                                if(err) { console.log(err); }
                                                if(foundOrder){
                                                    Order.findById(foundOrder.id, function (err, orderData) {
                                                        if (err){console.log(err);}
                                                        orderData.feedback.push(fdData.id);
                                                        orderData.save(function (err, updatedOrder) {
                                                            if (err){console.log(err);}
                                                            callb();
                                                        });
                                                    });
                                                }else{
                                                    callb();
                                                }
                                            })
                                        });
                                    }else{
                                        callb();
                                    }
                                });

                            }, function (err) {
                                if(err){
                                    console.log(err);
                                    console.log('there is a problem to get feedback data from server.');
                                }else{
                                    console.log('Feedback data saved to internal DB');
                                }
                            });
                        }else{
                            console.log('No report found yet.');
                        }
                    });
                }
            })
        });
    }


};


function formateSaveUpdateOrderData(resp, callback){
    if(resp && Object.keys(resp).length){
        if(resp.Orders.Order.length){
            async.each(resp.Orders.Order, function (order, callb) {
                Order.findOne({ AmazonOrderId: order.AmazonOrderId}, (err, existingId) => {
                    if (err) { console.log(err);}

                    // If order is exist
                    if (!existingId) {
                        order.sellerId = resp.sellerId;
                        const orderData = new Order(order);
                        orderData.save((err, sellerData) => {
                            if (err) { console.log(err); }
                            callb();
                        });
                    }else{
                        callb();
                    }
                });
            }, function (err) {
                if(err){
                    console.log(err);
                    callback({message: 'there is a problem to get data.'});
                }else{
                    callback({message: 'Order data has been updated'});
                }
            });
        }else{
            console.log('Did not response any data.');
            callback({message: 'Did not response any data.'});
        }
    }else{
        console.log('Did not response any data.');
        callback({message: 'Did not response any data.'});
    }
}
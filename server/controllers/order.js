const Order = require("../models/order");
const OrderItem = require("../models/orderItem");
const Template = require("../models/template");
const config = require("../config/main");
const moment = require("moment");
const Mail = require('./mail');

var scraper = require('../library/product-scraper');

const mws_key = config.MWS_KEY || "";
const mws_access = config.MWS_SECRET || "";
var amazonMws = require("amazon-mws")("AKIAIEGT53RIXYQUCTPQ", "VdToubCLaeVs+ngo3g7aIGCUzlqsisfCVWnKCga6");

exports.getOrder = function(req, res, next) {
  amazonMws.orders.search({
    "Version": "2013-09-01",
    "Action": "ListOrders",
    "SellerId": "A1LWZ980X488GK",
    "MWSAuthToken": "amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8",
    "MarketplaceId.Id.1": "ATVPDKIKX0DER",
    "LastUpdatedAfter": new Date(13, 12, 2017)
  }, function(error, response) {
    if (error) {
      console.log("error ", error);
      return;
    }

    return res.status(200).json(response);
    // var formData = formatOrderData(response);
    // update order table of Database as well as orderItem table

  });
};

exports.getOrderItem = function(req, res, next) {
  let orderId = req.params.id;
  amazonMws.orders.search({
    "Version": "2013-09-01",
    "Action": "ListOrderItems",
    "SellerId": "A1LWZ980X488GK",
    "MWSAuthToken": "amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8",
    "AmazonOrderId": orderId
  }, function(error, response) {
    if (error) {
      console.log("error ", error);
      return;
    }
    if (response && Object.keys(response).length) {

      
      var ASIN = response.OrderItems.OrderItem.ASIN;
      scraper.init('http://www.amazon.com/gp/product/'+ASIN+'/', function(data){
        response.OrderItems.OrderItem.image_url = data.image;
        formatOrderItem(response, function(respData) {

          OrderItem.findOne({ AmazonOrderId: respData.AmazonOrderId }, (err, existingItem) => {
            if (err) {
              console.log(err);
            }
            // If order is exist
            if (!existingItem) {
              const orderData = new OrderItem(respData);
              orderData.save((err, savedOrderItem) => {
                if (err) {
                  console.log(err);
                }
                updateOrderForItemStatus(savedOrderItem, function(updatedOrder) {
                  return res.status(200).json({ updated: updatedOrder });
                });
              });
            } else {
              return res.status(200).json({ exist: true, item: existingItem });
            }
          });
        }); 
      });
    }
  });
};

function updateOrderForItemStatus(respData, next) {
  Order.findOne({ AmazonOrderId: respData.AmazonOrderId }, function(err, order) {
    if (err) {
      console.log(err);
      return { status: 201, message: "something error", error: err };
    }
    order.haveItem = true;
    order.orderItem.push(respData.id);
    order.save(function(err, updatedOrder) {
      if (err) {
        console.log(err);
        return { error: err, code: 401, message: "something happened" };
      }
      next(updatedOrder);
    });
  });
}

function formatOrderItem(resp, calb) {
  let itemData = resp.OrderItems.OrderItem;
  let respOrder = {
    AmazonOrderId: resp.AmazonOrderId,
    asin: itemData.ASIN,
    qty: itemData.QuantityOrdered,
    sku: itemData.SellerSKU,
    title: itemData.Title,
    price: itemData.ItemPrice.Amount,
    currency: itemData.ItemPrice.CurrencyCode,
    image_url: itemData.image_url
  };
  calb(respOrder);
}

exports.list = function(req, res, next) {
  Order.find().populate("orderItem").limit(150).exec(function(err, orders) {
    if (err) {
      console.log(err);
    }
    return res.status(200).json({ orders: orders });
  });
};

exports.filterList = function(req, res, next) {
  let date_start = req.body.date_start;
  let date_end = req.body.date_end;
  let filter_items = req.body.filter_items;
  console.log(date_start, date_end, filter_items);

  let query = Order.find({});
  if ((date_start && date_end) && (filter_items && filter_items.length)) {
    date_start = moment(date_start, "YYYY/MM/DD").format("YYYY, M, D");
    date_end = moment(date_end, "YYYY/MM/DD").format("YYYY, M, D");
    // query.where = {PurchaseDate: {$gte: new Date(date_start), $lt: new Date(date_end)}};
    Order.find(
      {
        $and: [
          {
            PurchaseDate: {
              $gte: new Date(date_start), $lt: new Date(date_end)
            }
          },
          {
            $or: [
              {
                FulfillmentChannel: {
                  $in: filter_items
                }
              }, {
                OrderStatus: {
                  $in: filter_items
                }
              }]
          }
        ]
      }
    ).populate("orderItem")
      .limit(150).exec(function(err, orders) {
      if (err) {
        console.log(err);
      }
      return res.status(200).json({ orders: orders });
    });
  } else if (date_start && date_end) {
    date_start = moment(date_start, "YYYY/MM/DD").format("YYYY, M, D");
    date_end = moment(date_end, "YYYY/MM/DD").format("YYYY, M, D");
    Order.find({ PurchaseDate: { $gte: new Date(date_start), $lt: new Date(date_end) } }).populate("orderItem")
      .limit(150).exec(function(err, orders) {
      if (err) {
        console.log(err);
      }
      return res.status(200).json({ orders: orders });
    });
  } else if (filter_items && filter_items.length) {
    Order.find({ $or: [{ FulfillmentChannel: { $in: filter_items } }, { OrderStatus: { $in: filter_items } }] }).populate("orderItem")
      .limit(150).exec(function(err, orders) {
      if (err) {
        console.log(err);
      }
      return res.status(200).json({ orders: orders });
    });
  } else {
    Order.find().populate("orderItem")
      .limit(150).exec(function(err, orders) {
      if (err) {
        console.log(err);
      }
      return res.status(200).json({ orders: orders });
    });
  }
};


function formatOrderData(resp) {
  let orderData = { NextToken: resp.NextToken, RequestId: resp.ResponseMetadata.RequestId };
  orderData.orders = resp.Orders.Order;
  return orderData;
}


exports.sendMailToOrderer = function (req, res) {

  const orderId = req.body.orderId;

  Order.findOne({AmazonOrderId: orderId}, function (err, Order) {
    if (err) {
      console.log(err);
    } else {
      Template.findOne({order_id: orderId}, function (error,temp) {
        if (err) {
          console.log(error);
        } else {

          if (temp !== null) {

                let obj = {
                  email: Order.BuyerEmail,
                  //email: 'rasel.fbt@gmail.com',
                    templateData: temp
                };
                Mail.sendToOrderer(obj,function (result) {
                  if (result) {
                    temp.﻿number_of_email_sent += 1;
                    temp.save(function (err, success) {
                        if (err) {
                          res.status(403).send(err);
                        } else {
                          res.send({success: true, number_of_email_sent: success.number_of_email_sent});
                        }
                    });
                  }
                });

          } else {
            res.send({ success: false, message: " No template found" })
          }

        }

      });

    }

  });

}


exports.sendScheduleLetter = function (req, res) {

    Template.find({template_status: 'active'})
        .exec(function (err,Temp) {
      if (err) {
        console.log(err);
      } else {

        Temp.forEach(function (temp,index) {

            Order.findOne({AmazonOrderId: temp.order_id}).select('OrderStatus BuyerEmail').exec(function (error,Ord) {
                if (error) {
                  console.log(error);
                } else {

                    let timeNow = moment().format('kk:mm');

                    let a = timeNow.split(':'); // split it at the colons
                    let minutesNow = (+a[0]) * 60 + (+a[1]);

                    let b = temp.send_time.split(':');
                    let templateTime = (+b[0]) * 60 + (+b[1]);

                    let distance = minutesNow - templateTime;

                    if((Ord.OrderStatus === temp.send_after) && (distance<59)) {

                        let obj = {
                            email: Ord.BuyerEmail,
                            //email: 'rasel.fbt@gmail.com',
                            templateData: temp
                        };
                        Mail.sendToOrderer(obj,function (result) {
                            if (result) {
                              console.log('sent emil')
                                temp.﻿number_of_email_sent += 1;
                                temp.save(function (err, success) {
                                    if (err) return;
                                    return;
                                });
                            }
                        });

                    }
                }
            });
        });


        res.send('Schedule email sent to user');

      }

    }).catch(error=>{console.log(error)})
}

const SellerConfig = require('../models/sellerConfig');
const Cron = require('../models/cron');
const config = require('../config/main');


//= =======================================
// User Routes
//= =======================================
// exports.viewProfile = function (req, res, next) {
//     const userId = req.params.userId;
//
//     if (req.user._id.toString() !== userId) { return res.status(401).json({ error: 'You are not authorized to view this user profile.' }); }
//     User.findById(userId, (err, user) => {
//         if (err) {
//             res.status(400).json({ error: 'No user could be found for this ID.' });
//             return next(err);
//         }
//
//         const userToReturn = setUserInfo(user);
//
//         return res.status(200).json({ user: userToReturn });
//     });
// };


exports.orderConfig = function (req, res, next) {

    SellerConfig.findOne({ SellerId: 'A1LWZ980X488GK'}, (err, existingId) => {
        if (err) { return next(err); }

        // If user is not unique, return error
        if (existingId) {
            return res.status(422).send({ error: 'That seller id is already exist.' });
        }

        const sellerData = new SellerConfig({SellerId: 'A1LWZ980X488GK', tokenConfig: {'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8',
                'id1': 'ATVPDKIKX0DER'}});

        sellerData.save((err, sellerData) => {
            if (err) { console.log(err); return next(err); }
            console.log(sellerData);
            return res.status(200).json(sellerData);
        });
    });
};

exports.cron = function (req, res, next) {

    Cron.findOne({ SellerId: 'A1LWZ980X488GK'}, (err, existingId) => {
        if (err) { return next(err); }

        // If user is not unique, return error
        if (existingId) {
            return res.status(422).send({ error: 'That seller id is already exist for cron.' });
        }

        const cronData = new Cron({SellerId: 'A1LWZ980X488GK', type: 'order', count: 0, current_date: null});

        cronData.save((err, cronData) => {
            if (err) { console.log(err); return next(err); }
            console.log(cronData);
            return res.status(200).json(cronData);
        });
    });
};

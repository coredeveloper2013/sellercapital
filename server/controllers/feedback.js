const Order = require("../models/order");
const Feedback = require("../models/feedback");
const Product = require("../models/product");
const OrderItem = require("../models/orderItem");
const config = require("../config/main");
const moment = require("moment");
const async = require("async");

const mws_key = config.MWS_KEY || "";
const mws_access = config.MWS_SECRET || "";
var amazonMws = require("amazon-mws")("AKIAIEGT53RIXYQUCTPQ", "VdToubCLaeVs+ngo3g7aIGCUzlqsisfCVWnKCga6");

exports.getReportList = function(req, res, next) {
  amazonMws.reports.search({
    "Version": "2009-01-01",
    "Action": "GetReportList",
    "SellerId": "A1LWZ980X488GK",
    "MWSAuthToken": "amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8"
    //'ReportTypeList.Type.1': 'REPORT_TYPE_LIST' //optional
  }, function(error, response) {
    if (error) {
      console.log("error ", error);
      // return;
      return res.status(200).json(error);
    }
    return res.status(200).json(response);
    // console.log('response', response);
  });
};

exports.getReportRequestList = function(req, res, next) {
  amazonMws.reports.search({
    "Version": "2009-01-01",
    "Action": "GetReportRequestList",
    "SellerId": "A1LWZ980X488GK",
    "MWSAuthToken": "amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8"
    //'ReportTypeList.Type.1': 'REPORT_TYPE_LIST' //optional
  }, function(error, response) {
    if (error) {
      console.log("error ", error);
      // return;
      return res.status(200).json(error);
    }
    return res.status(200).json(response);
    // console.log('response', response);
  });
};


exports.getReport = function(req, res, next) {
  let reportId = req.params.id;
  let sellerId = "A1LWZ980X488GK";
  amazonMws.reports.search({
    "Version": "2009-01-01",
    "Action": "GetReport",
    "SellerId": sellerId,
    "MWSAuthToken": "amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8",
    "ReportId": reportId
    //'ReportTypeList.Type.1': 'REPORT_TYPE_LIST' //optional
  }, function(error, response) {
    if (error) {
      console.log(error);
    }
    let respData = response.data || [];
    if (respData.length) {
      let feedbackData = respData.map(rd => {
        return {
          date: rd["Date"], rating: rd["Rating"], comment: rd["Comments"], sellerId: sellerId,
          response: rd["Your Response"], orderId: rd["Order ID"], email: rd["Rater Email"]
        };
      });

      async.each(feedbackData, function(feedbackDta, callb) {
        Feedback.findOne({
          sellerId: sellerId,
          orderId: feedbackDta.orderId,
          email: feedbackDta.email
        }, (err, foundFeedback) => {
          if (err) {
            console.log(err);
          }
          // If order is exist
          if (!foundFeedback) {
            const feedbackData = new Feedback(feedbackDta);
            feedbackData.save((err, fdData) => {
              if (err) {
                console.log(err);
              }
              // console.log(fdData);
              Order.findOne({ sellerId: fdData.sellerId, AmazonOrderId: fdData.orderId }, (err, foundOrder) => {
                if (err) {
                  console.log(err);
                }
                if (foundOrder) {
                  Order.findById(foundOrder.id, function(err, orderData) {
                    if (err) {
                      console.log(err);
                    }
                    orderData.haveFeedback = true;
                    orderData.feedback.push(fdData.id);
                    orderData.save(function(err, updatedOrder) {
                      if (err) {
                        console.log(err);
                      }
                      callb();
                    });
                  });
                } else {
                  callb();
                }
              });
            });
          } else {
            callb();
          }
        });

      }, function(err) {
        if (err) {
          console.log(err);
          console.log("there is a problem to get feedback data from server.");
        } else {
          return res.status(200).json({ message: "successfully done" });
        }
      });
    } else {
      return res.status(200).json(response);
    }
  });
};


exports.requestReport = function(req, res, next) {
  let startDate = moment().subtract(3, "years").toISOString();
  let endDate = moment().subtract(2, "years").toISOString();
  amazonMws.reports.search({
    "Version": "2009-01-01",
    "Action": "RequestReport",
    "SellerId": "A1LWZ980X488GK",
    "MWSAuthToken": "amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8",
    // 'MarketplaceIdList.Id.1': 'ATVPDKIKX0DER',
    "ReportType": "_GET_SELLER_FEEDBACK_DATA_",
    "StartDate": startDate,
    "EndDate": endDate
    //'ReportTypeList.Type.1': 'REPORT_TYPE_LIST' //optional
  }, function(error, response) {
    if (error) {
      console.log("error ", error);
      // return;
      return res.status(200).json(error);
    }
    return res.status(200).json(response);
    // console.log('response', response);
  });
};

exports.getAllFeedback = function(req, res, next) {
  const reviewsCrawler = require("amazon-reviews-crawler");
  const sellerId = "A1LWZ980X488GK";
  let productId;
  Product.find({ sellerId: sellerId }).limit(150).exec(function(err, products) {
    if (err) {
      console.log(err);
    }
    console.log("products found ", products.length);
    async.each(products, function(product, callb) {
      productId = product.product_id;
      reviewsCrawler(productId)
        .then(function(results) {

          let feedbacks = results.reviews || [];
          console.log("review found ", feedbacks.length);
          feedbacks.sellerId = sellerId;
          feedbacks.product_id = productId;
          feedbacks.product_title = results.title;
          saveAndBackFeedback(feedbacks, function(data) {
            console.log("reply back from save and back");
            if (data) {
              return callb();
            }
          });

        }).catch(function(err) {
        console.error(err);
        // return callb();
      });

    }, function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("get all feedback process done !");
        Feedback.find().limit(150).exec(function(err, feedbacks) {
          if (err) {
            console.log(err);
          }
          console.log("final return get all feedback !");
          return res.status(200).json({ feedback: feedbacks });
        });
      }
    });
  });
};

function saveAndBackFeedback(feedbacks, callback) {
  console.log("save and back feedback in");
  let feedbackData;

  async.each(feedbacks, function(feedback, callb) {
    Feedback.findOne({ feedback_id: feedback.id }, (err, existingItem) => {
      if (err) {
        console.log(err);
      }
      if (existingItem) {
        return callb();
      } else {
        feedbackData = {
          product_title: feedbacks.product_title || "", title: feedback.title, date: new Date(feedback.date)
          , link: feedback.link, rating: feedback.rating, comment: feedback.text, author: feedback.author
        };

        feedbackData.sellerId = feedbacks.sellerId;
        feedbackData.product_id = feedbacks.product_id;

        const productDataObj = new Feedback(feedbackData);
        productDataObj.save((err, savedItem) => {
          console.log("Feedback data saved");
          if (err) {
            console.log(err);
          }
          return callb();
        });
      }
    });

  }, function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log("save and back feedback done");
      callback({ data: "done" });
    }
  });


  feedbacks.forEach(function(feedback) {
    feedbackData = {
      product_title: feedbacks.product_title || "", title: feedback.title, date: new Date(feedback.date)
      , link: feedback.link, rating: feedback.rating, comment: feedback.text, author: feedback.author
    };

    feedbackData.sellerId = feedbacks.sellerId;
    feedbackData.product_id = feedbacks.product_id;

    const productDataObj = new Feedback(feedbackData);
    productDataObj.save((err, savedItem) => {
      if (err) {
        console.log(err);
      }
      return callback();
    });
  });
}

exports.getProductFeedback = function(req, res, next) {
  const axios = require("axios");
  const request = require("request");
  const rp = require("request-promise");
  const cheerio = require("cheerio");
  let fs = require("fs");
  let productId = req.params.id;
  let sellerId = "A1LWZ980X488GK";
  const reviewsCrawler = require("amazon-reviews-crawler");

  console.log("got the product id", productId);

  let url = `https://www.amazon.com/product-reviews/${productId}/ref=cm_cr_arp_d_viewopt_srt?reviewerType=all_reviews&pageNumber=1&sortBy=recent`;

  let rData = [];
  let iteration = 0;

  let options;


  options = {
    uri: `https://medium.com/`,
    transform: function(body) {
      return cheerio.load(body);
    }
  };
  rp(options)
    .then(($) => {
      console.log($);
      const logoData = $(".siteNav-logo").text();
      return res.status(200).json({ data: logoData });
    })
    .catch((err) => {
      console.log(err);
      return res.status(200).json({ data: err });
    });

  // function getRequestToAmazon() {
  //
  // }
  //
  // getRequestToAmazon(); // execute function


  // reviewsCrawler(productId)
  //     .then(function(results){
  //
  //         console.log(results);
  //
  //         let feedbacks = results.reviews || [], feedbackData;
  //         console.log('found feedback', feedbacks.length);
  //
  //         async.each(feedbacks, function (feedback, callb) {
  //             Feedback.findOne({ feedback_id: feedback.id }, (err, existingItem) => {
  //                 if (err) { console.log(err);}
  //                 if (existingItem) {
  //                     return callb();
  //                 }else{
  //                     feedbackData = {product_title: results.title || '', title: feedback.title, date: new Date(feedback.date)
  //                     , link: feedback.link, rating: feedback.rating, comment: feedback.text, author: feedback.author};
  //
  //                     feedbackData.sellerId = sellerId;
  //                     feedbackData.product_id = productId;
  //
  //                     const productDataObj = new Feedback(feedbackData);
  //                     productDataObj.save((err, savedItem) => {
  //                         console.log('feedback saved');
  //                         if (err) { console.log(err); }
  //                         return callb();
  //                     });
  //                 }
  //             });
  //
  //         }, function (err) {
  //             if(err){
  //                 console.log(err);
  //             }else{
  //                 Feedback.find().limit(150).exec(function (err, feedbacks) {
  //                     console.log('All feedback process done, total feedbacks ', feedbacks.length);
  //                     if (err){console.log(err);}
  //                     return res.status(200).json({ feedback: feedbacks });
  //                 })
  //             }
  //         });
  //     })
  //     .catch(function(err){
  //         console.log('error here');
  //         console.log(err);
  //         console.error(err)
  //     })

};


exports.getOrderItem = function(req, res, next) {
  let orderId = req.params.id;
  amazonMws.orders.search({
    "Version": "2013-09-01",
    "Action": "ListOrderItems",
    "SellerId": "A1LWZ980X488GK",
    "MWSAuthToken": "amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8",
    "AmazonOrderId": orderId
  }, function(error, response) {
    if (error) {
      console.log("error ", error);
      return;
    }
    if (response && Object.keys(response).length) {
      formatOrderItem(response, function(respData) {
        OrderItem.findOne({ AmazonOrderId: respData.AmazonOrderId }, (err, existingItem) => {
          if (err) {
            console.log(err);
          }
          // If order is exist
          if (!existingItem) {
            const orderData = new OrderItem(respData);
            orderData.save((err, savedOrderItem) => {
              if (err) {
                console.log(err);
              }
              updateOrderForItemStatus(savedOrderItem, function(updatedOrder) {
                return res.status(200).json({ updated: updatedOrder });
              });
            });
          } else {
            return res.status(200).json({ exist: true, item: existingItem });
          }
        });
      });
    }
  });
};

function updateOrderForItemStatus(respData, next) {
  Order.findOne({ AmazonOrderId: respData.AmazonOrderId }, function(err, order) {
    if (err) {
      console.log(err);
      return { status: 201, message: "something error", error: err };
    }
    order.haveItem = true;
    order.orderItem.push(respData.id);
    order.save(function(err, updatedOrder) {
      if (err) {
        console.log(err);
        return { error: err, code: 401, message: "something happened" };
      }
      next(updatedOrder);
    });
  });
}

function formatOrderItem(resp, calb) {
  let itemData = resp.OrderItems.OrderItem;
  let respOrder = {
    AmazonOrderId: resp.AmazonOrderId,
    asin: itemData.ASIN,
    qty: itemData.QuantityOrdered,
    sku: itemData.SellerSKU,
    title: itemData.Title,
    price: itemData.ItemPrice.Amount,
    currency: itemData.ItemPrice.CurrencyCode
  };
  calb(respOrder);
}

exports.list = function(req, res, next) {
  Feedback.find().limit(150).exec(function(err, feedback) {
    if (err) {
      console.log(err);
    }
    const feedbackData = feedback.map(function(fd) {
      return {
        feedback: { title: fd.title, details: fd.comment }, rating: fd.rating, author: fd.author,
        product_info: { id: fd.product_id, title: fd.product_title }, date: fd.date
      };
    });
    Feedback.find(
          { $or: [{ rating: "1" }, { rating: "2"}] }
      ).limit(150).exec(function(err, totalNegativeFeedback) {
          if (err){console.log(err);}
          console.log("totalNegativeFeedback "+ totalNegativeFeedback.length)
          Feedback.find(
              { rating: "3" }
          ).limit(150).exec(function(err, totalNeutralFeedback) {
              if (err){console.log(err);}
              
              console.log("totalNeutralFeedback "+ totalNeutralFeedback.length)
              
              return res.status(200).json({ totalNegativeFeedback: totalNegativeFeedback.length, totalNeutralFeedback: totalNeutralFeedback.length, feedback: feedbackData });
          });
      });
  });
};

exports.filterList = function(req, res, next) {
  let date_start = req.body.date_start;
  let date_end = req.body.date_end;
  let filter_items = req.body.filter_items;
  console.log(date_start, date_end, filter_items);

  let query = Order.find({ haveFeedback: true });


  Feedback.find(
          { $or: [{ rating: "1" }, { rating: "2"}] }
    ).limit(150).exec(function(err, totalNegativeFeedback) {
        if (err){console.log(err);}
        console.log("totalNegativeFeedback "+ totalNegativeFeedback.length)
        Feedback.find(
            { rating: "3" }
        ).limit(150).exec(function(err, totalNeutralFeedback) {
            if (err){console.log(err);}
            
            console.log("totalNeutralFeedback "+ totalNeutralFeedback.length)

            if ((date_start && date_end) && (filter_items && filter_items.length)) {

              date_start = moment(date_start, "YYYY/MM/DD").format("YYYY, M, D");
              date_end = moment(date_end, "YYYY/MM/DD").format("YYYY, M, D");
              Feedback.find(
                {
                  $and: [
                    {
                      date: {
                        $gte: new Date(date_start), $lt: new Date(date_end)
                      }
                    },
                    {
                      $or: [
                        {
                          rating: {
                            $in: filter_items
                          }
                        }
                      ]
                    }
                  ]
                }
              ).limit(150).exec(function(err, feedback) {
                if (err) {
                  console.log(err);
                }
                const feedbackData = feedback.map(function(fd) {
                  return {
                    feedback: { title: fd.title, details: fd.comment }, rating: fd.rating, author: fd.author,
                    product_info: { id: fd.product_id, title: fd.product_title }, date: fd.date
                  };
                });
                return res.status(200).json({ totalNegativeFeedback: totalNegativeFeedback.length, totalNeutralFeedback: totalNeutralFeedback.length, feedback: feedbackData });
              });
            } else if (date_start && date_end) {
              date_start = moment(date_start, "YYYY/MM/DD").format("YYYY, M, D");
              date_end = moment(date_end, "YYYY/MM/DD").format("YYYY, M, D");
              Feedback.find(
                { date: { $gte: new Date(date_start), $lt: new Date(date_end) } }
              ).limit(150).exec(function(err, feedback) {
                if (err) {
                  console.log(err);
                }
                const feedbackData = feedback.map(function(fd) {
                  return {
                    feedback: { title: fd.title, details: fd.comment }, rating: fd.rating, author: fd.author,
                    product_info: { id: fd.product_id, title: fd.product_title }, date: fd.date
                  };
                });
                return res.status(200).json({ totalNegativeFeedback: totalNegativeFeedback.length, totalNeutralFeedback: totalNeutralFeedback.length, feedback: feedbackData });
              });
            } else if (filter_items && filter_items.length) {
              console.log("filter items "+filter_items);
              Feedback.find(
                { $or: [{ rating: { $in: filter_items } }] }
              ).limit(150).exec(function(err, feedback) {
                if (err) {
                  console.log(err);
                }
                const feedbackData = feedback.map(function(fd) {
                  return {
                    feedback: { title: fd.title, details: fd.comment }, rating: fd.rating, author: fd.author,
                    product_info: { id: fd.product_id, title: fd.product_title }, date: fd.date
                  };
                });
                return res.status(200).json({ totalNegativeFeedback: totalNegativeFeedback.length, totalNeutralFeedback: totalNeutralFeedback.length, feedback: feedbackData });
              });

            } else {
              Feedback.find().limit(150).exec(function(err, feedback) {
                if (err) {
                  console.log(err);
                }
                const feedbackData = feedback.map(function(fd) {
                  return {
                    feedback: { title: fd.title, details: fd.comment }, rating: fd.rating, author: fd.author,
                    product_info: { id: fd.product_id, title: fd.product_title }, date: fd.date
                  };
                });
                return res.status(200).json({ totalNegativeFeedback: totalNegativeFeedback.length, totalNeutralFeedback: totalNeutralFeedback.length, feedback: feedbackData });
              });
            }
        });
    });
};


function formatOrderData(resp) {
  let orderData = { NextToken: resp.NextToken, RequestId: resp.ResponseMetadata.RequestId };
  orderData.orders = resp.Orders.Order;
  return orderData;
}

exports.getFeedbackAnalysis = function(req, res, next) {
  Feedback.aggregate([
        { $group: {
            _id: {$substr: ['$date', 0, 7]}, 
            totalFeedback: {$sum: 1}
        }}
    ], function (err, feedback) {
        if (err) {
        console.log(err);
      }
      return res.status(200).json(feedback);
    });
};
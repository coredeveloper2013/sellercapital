# MERN Starter
Starter/seed project for MongoDB, Express, React, Node full-stack JavaScript apps.

It substitutes Koa in for Express, and features more modern syntax, like async/await. I would recommend taking a look at that project instead.

## Contributions
Please feel free to contribute to this project. Whether it's features, tests, or code cleanup, any help is welcome at this point.

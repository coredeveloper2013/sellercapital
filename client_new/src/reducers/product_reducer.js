import { FETCH_PRODUCTS, FETCH_SINGLE_PRODUCT, PRODUCT_ERROR } from '../ducks/types';

const INITIAL_STATE = { products: [], product: '', error: '', totalNegativeReview: 0 };

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_PRODUCTS:
            return {
             ...state,
             products: action.payload.products,
             totalNegativeReview: action.payload.totalNegativeReview
           };
        case FETCH_SINGLE_PRODUCT:
            return { ...state, product: action.payload.product };
        case PRODUCT_ERROR:
            return { ...state, error: action.payload };
        default:
    }
    return state;
}

import {
  FETCH_ORDERS,
  FETCH_SINGLE_ORDER,
  ORDER_ERROR,
  ORDER_ANALYSIS,
  ORDER_ANALYSIS_LOADER,
} from '../ducks/types';

const INITIAL_STATE = {
  orders: [],
  order: '',
  error: '',
  orderAnalysis: [],
  orderAnalysisLoader: false,
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_ORDERS:
            return { ...state, orders: action.payload.orders };
        case ORDER_ANALYSIS:
            return { ...state, orderAnalysis: action.payload.order_analysis };
        case ORDER_ANALYSIS_LOADER:
          return {...state, orderAnalysisLoader: action.payload}
        case FETCH_SINGLE_ORDER:
            return { ...state, order: action.payload.order };
        case ORDER_ERROR:
            return { ...state, error: action.payload };
        default:
    }

    return state;
}

import {
  FETCH_FEEDBACK,
  FETCH_SINGLE_FEEDBACK,
  FEEDBACK_ERROR,
  FEEDBACK_ANALYSIS,
  FEEDBACK_ANALYSIS_LOADER,
} from '../ducks/types';

const INITIAL_STATE = {
  feedback: [],
  single_feedback: {},
  error: '',
  totalNegativeFeedback: 2,
  totalNeutralFeedback: 2,
  feedbackAnalysis: [],
  feedbackAnalysisLoader: false,
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_FEEDBACK:
            return {
              ...state,
              feedback: action.payload.feedback,
              totalNeutralFeedback: action.payload.totalNeutralFeedback,
              totalNegativeFeedback: action.payload.totalNegativeFeedback,
             };
        case FETCH_SINGLE_FEEDBACK:
            return { ...state, single_feedback: action.payload.feedback };
        case FEEDBACK_ANALYSIS:
          return {...state, feedbackAnalysis: action.payload.feedback_analysis}
        case FEEDBACK_ANALYSIS_LOADER:
          return {...state, feedbackAnalysisLoader: action.payload}
        case FEEDBACK_ERROR:
            return { ...state, error: action.payload };
        default:
    }

    return state;
}

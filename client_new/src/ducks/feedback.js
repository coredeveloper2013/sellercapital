import {
    API_URL,
    CLIENT_ROOT_URL,
    FETCH_ORDERS,
    FETCH_FEEDBACK,
    FEEDBACK_ERROR,
    FETCH_SINGLE_ORDER,
    ORDER_ERROR,
    FETCH_PRODUCTS, PRODUCT_ERROR,
    FEEDBACK_ANALYSIS,
    FEEDBACK_ANALYSIS_LOADER,
} from "./types";

// for crud operation
import {
  getData,
  postData,
  putData,
  deleteData,
  getDataAnalysis,
} from './index';


export function fetchFeedback() {
    const url = '/feedback';
    const type = {
      actionType: FETCH_FEEDBACK,
      errorType: FEEDBACK_ERROR,
      loaderType: null,
    }

    return function (dispatch) {
        getData(type, true, url, function (respData) {
            return dispatch(respData);
        })
    }
}

export function requestForUpdateReport(){
    const url = '/feedback_report_request';
    const type = {
      actionType: FETCH_FEEDBACK,
      errorType: FEEDBACK_ERROR,
      loaderType: null,
    }
    return dispatch => getData(type, true, url, dispatch);
}

export function fetchFilteredFeedback_Old(filter) {
    // console.log(filter);
    const url = '/feedback';
    // return dispatch => postData(FETCH_ORDERS, ORDER_ERROR, true, url, dispatch, filter);
    const type = {
      actionType: FETCH_FEEDBACK,
      errorType: FEEDBACK_ERROR,
      loaderType: null,
    }
    return function (dispatch) {
        getData(type, true, url, function (respData) {
            return dispatch(respData);
        })
    }
}

export function fetchFeedbackFilters(filter) {
    const url = '/feedback';
    const type = {
      actionType: FETCH_FEEDBACK,
      errorType: FEEDBACK_ERROR,
      loaderType: null,
    }
    return dispatch => postData(type, true, url, dispatch, filter);
}
export function fetchFeedbackAnalysis(params) {
    let url = '/feedback_analysis';
    if (params) {
      url += params
    }
    const type = {
      actionType: FEEDBACK_ANALYSIS,
      errorType: FEEDBACK_ERROR,
      loaderType: FEEDBACK_ANALYSIS_LOADER,
    }
    return dispatch => getDataAnalysis(type, true, url, dispatch);
}


// export const REDUCER = 'order'

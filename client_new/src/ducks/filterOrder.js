import { createAction, createReducer } from 'redux-act'
import cookie from 'react-cookie';
import {API_URL, CLIENT_ROOT_URL, AUTH_ERROR, AUTH_USER, FETCH_ORDERS, ORDER_ERROR} from "./types";
import {getData} from "./index";


const REDUCER = 'filterOrder'
const NS = `@@${REDUCER}/`


const initialState = {
    // APP STATE
    orders: []
}

export function fetchOrders() {
    const url = '/orders';
    const type = {
      actionType: FETCH_ORDERS,
      errorType: ORDER_ERROR,
      loaderType: null,
    }
    // return dispatch => getData(FETCH_ORDERS, ORDER_ERROR, true, url, dispatch);
    return function (dispatch) {
        getData(type, true, url, function (respData) {
            return dispatch(respData);
        })
    }
}


// export const fetchOrders = createAction(`${NS}FETCH_ORDER_DATA`)

export default createReducer(
    {
        // [fetchOrders]: () => {
        //     const url = '/orders';
        //     return 'something'
        //     // return dispatch => getData(FETCH_ORDERS, ORDER_ERROR, true, url, dispatch);
        //     // return function (dispatch) {
        //     //     getData(FETCH_ORDERS, ORDER_ERROR, true, url, function (respData) {
        //     //         console.log(respData);
        //     //         return dispatch(respData);
        //     //     })
        //     // }
        // },
    },
    initialState,
)

import {
  API_URL,
  CLIENT_ROOT_URL,
  FETCH_ORDERS,
  FETCH_SINGLE_ORDER,
  ORDER_ERROR,
  ORDER_ANALYSIS,
  ORDER_ANALYSIS_LOADER,
} from "./types";


// for crud operation
import {
  getData,
  postData,
  putData,
  deleteData,
  getDataAnalysis,
} from './index';


export function fetchOrders() {
    const url = '/orders';
    const type = {
      actionType: FETCH_ORDERS,
      errorType: ORDER_ERROR,
      loaderType: null,
    }
    return dispatch => getData(type, true, url, dispatch);
}

export function fetchFilteredOrders(filter) {
    const url = '/orders';
    const type = {
      actionType: FETCH_ORDERS,
      errorType: ORDER_ERROR,
      loaderType: null,
    }
    return dispatch => postData(type, true, url, dispatch, filter);
}

export function fetchOrderAnalysis(params) {
    let url = '/order_analysis';
    if (params) {
      url += params
    }
    const type = {
      actionType: ORDER_ANALYSIS,
      errorType: ORDER_ERROR,
      loaderType: ORDER_ANALYSIS_LOADER,
    }
    return dispatch => getDataAnalysis(type, true, url, dispatch);
}


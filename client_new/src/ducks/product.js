import {API_URL, CLIENT_ROOT_URL, FETCH_PRODUCTS, FETCH_SINGLE_PRODUCT, PRODUCT_ERROR} from "./types";


// for crud operation
import { getData, postData, putData, deleteData } from './index';


export function fetchProducts() {
    const url = '/products';
    const type = {
      actionType: FETCH_PRODUCTS,
      errorType: PRODUCT_ERROR,
      loaderType: null,
    }
    return dispatch => getData(type, true, url, dispatch);
}

export function requestForUpdateReport(){
    const url = '/product_report_request';
    const type = {
      actionType: FETCH_PRODUCTS,
      errorType: PRODUCT_ERROR,
      loaderType: null,
    }
    return dispatch => getData(type, true, url, dispatch);
}

export function fetchFilteredOrders(filter) {
    const url = '/orders';
    const type = {
      actionType: FETCH_PRODUCTS,
      errorType: PRODUCT_ERROR,
      loaderType: null,
    }
    return dispatch => postData(type, true, url, dispatch, filter);
}
export function fetchProductFilters(filter) {
  const url = '/products'
    const type = {
      actionType: FETCH_PRODUCTS,
      errorType: PRODUCT_ERROR,
      loaderType: null,
    }
  return dispatch => postData(type, true, url, dispatch, filter);
}


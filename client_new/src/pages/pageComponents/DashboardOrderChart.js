import React from 'react';
import {Spin} from 'antd'
import {Line} from 'react-chartjs-2';
import pageData from '../pageData/index.js'


export default class DashboardOrderChart extends React.Component {
  render () {
    const {orderAnalysisLoader, orderAnalysis} = this.props
    let orderAnalysisData = pageData.transformAnalysisData({
        analysisData: this.props.orderAnalysis,
        key: 'totalOrder',
        label: 'Number of Orders',
        chartName: 'line',
      })
    let lineOptions = {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            padding: 25,
            beginAtZero: true,
          }
        }]
      }
    }
    return orderAnalysisLoader ?
    <div className="text-center" >
      <Spin />
    </div> : ( orderAnalysis.length > 1 ?

    <Line
    height={400}
    options={lineOptions}
    data={orderAnalysisData} /> : <h2>No Order found</h2>
    )


  }

}

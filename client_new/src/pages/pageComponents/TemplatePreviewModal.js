import React, {Component} from 'react';
import {
  Modal,
  Input,
  Button
} from 'antd';
// import ids from 'short-id'

class TemplatePreviewModal extends Component {
  state = {
    userEmail: '',
    emailError: false,
    showTemplatePreview: false,
  }
  handleCTemplateCancel = () => {
    this.setState({
      showTemplatePreview: false
    });
  }

  validateEmail = (email)  => {
    const re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    return re.test(email);
  }
  sendTestMail = () => {
    const {userEmail}  = this.state
    let payload = {
      "email": userEmail
    }
    // console.log('payload', payload)
    if (this.validateEmail(userEmail)) {
      this.setState({emailError: false})
      // this.props.sendTestMail(payload)
      // this is the place where I need call api call
      this.props.sendTestMail(payload)
      this.handleCTemplateCancel()
    } else {
      this.setState({emailError: true})
    }
  }
  previewTemplate = () => {
    this.setState({
      showTemplatePreview: true
    })
  }
  render () {
    console.log('this.props', this.props)
    const {showTemplatePreview} = this.state
    const {template_name, templateOutput} = this.props
    return(
    <div className='d-inline m-2'>
      <Button
        type="primary"
        onClick={this.previewTemplate}>PreviewTemplate
      </Button>
      <Modal title={template_name}
             visible={showTemplatePreview}
        // onOk={this.submitCreateTemplate}
             className={"centered"}
             maskClosable={false}
             onCancel={this.handleCTemplateCancel} width="790px"
             footer={[
                <div key="hello world 2" className='d-flex justify-content-between align-items-start'>
                  <div className="d-inline-block">
                    <Input onChange={(e) => this.setState({userEmail: e.target.value})} className="d-inline mr-2" placeholder="Enter your email address" />
                    {
                      this.state.emailError ?
                        <label className='text-danger'>Please add a valid email address</label> : null
                    }
                  </div>
                  <div>
                    <Button key='sendTestMail' icon='mail' onClick={this.sendTestMail}>Send Test
                      Mail</Button>
                    <Button key='backToTemplate' onClick={this.handleCTemplateCancel}>Return</Button>
                  </div>
                </div>
             ]}>

        <div dangerouslySetInnerHTML={{ __html: templateOutput }}></div>

      </Modal>
    </div>
    )
  }
}


export default TemplatePreviewModal;

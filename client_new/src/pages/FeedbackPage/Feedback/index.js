import React from "react";
import {
  Table,
  Icon,
  Input,
  Button,
  DatePicker,
  Select,
  Radio,
  Rate,
  Menu,
  Dropdown,
  notification,
  Modal,
  Row,
  Col,
  Alert,
} from "antd";
import moment from "moment";
import { connect } from "react-redux";
import { fetchFeedback, requestForUpdateReport, fetchFeedbackFilters } from "./../../../ducks/feedback";
import { setLoading } from "./../../../ducks/app";
import "./style.scss";
import { REDUCER, submit } from "../../../ducks/profile";
import pageData from "../../pageData";
import ids from 'short-id'


const RangePicker = DatePicker.RangePicker;
const { Option, OptGroup } = Select;
const confirm = Modal.confirm;


const defaultPagination = {
  pageSizeOptions: ["50", "100", "250"],
  showSizeChanger: true,
  current: 1,
  size: "small",
  showTotal: (total: number) => `Total ${total} items`,
  total: 0, pageSize: 50
};
let allFilters = pageData.filteringOptionsFeedback.map((option, i) =>
  <OptGroup key={i} label={option.title}>
    {
      option.keys.map((op, j) =>
        <Option key={op.title} value={op.value}>{op.title}</Option>
      )
    }
  </OptGroup>
);


const children = [];
let optionKeys = [{ key: "AFN", value: "AFN" }, { key: "MFN", value: "MFN" }, { key: "Shipped", value: "Shipped" },
  { key: "Pending", value: "Pending" }, { key: "Unshipped", value: "Unshipped" },
  { key: "Delivered", value: "Delivered" }, { key: "Canceled", value: "Canceled" }, {
    key: "Returned",
    value: "Returned"
  }];
for (let i = 0; i < optionKeys.length; i++) {
  children.push(<Option key={optionKeys[i].key}>{optionKeys[i].value}</Option>);
}


const mapStateToProps = (state, props) => ({
  feedback: state.feedback.feedback,
  totalNeutralFeedback: state.feedback.totalNeutralFeedback,
  totalNegativeFeedback: state.feedback.totalNegativeFeedback,

});


const mapDispatchToProps = (dispatch, props) => ({
  fetchFeedback: dispatch(fetchFeedback()),
  getUpdateFeedback: () => {
    dispatch(requestForUpdateReport());
  },
  fetchFeedbackFilters: filter => {
      dispatch(fetchFeedbackFilters(filter))
  }
});


@connect(mapStateToProps, mapDispatchToProps)

class Feedback extends React.Component {

  componentWillMount() {
    // Fetch inbox (conversations involving current user)
    // this.props.fetchOrders();
    // setLoading(true);
  }

  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: "",
    filtered: false,
    loading: true
  };

  onClickFilterSearch = e => {
    const { date_start, date_end, filter_items } = this.state;
    let filter_data = this.props.fetchFeedbackFilters({ date_start, date_end, filter_items });

    console.log(filter_data);
    // console.log(this.props.filterOrder);
  };

  onDatePickerChange = (date, dateString) => {
    if (dateString && dateString.length) {
      this.setState({ date_start: dateString[0], date_end: dateString[1] });
    }
  };

  filterItemChange = values => {
    this.setState({ filter_items: values });
  };


  handleSizeChange = (e) => {
    this.setState({ size: e.target.value });
  };

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };

  setFeedbackProduct = (e) => {
    const self = this;
    confirm({
      title: "Are you sure to sent request to get report?",
      content: "It will sent a request to MWS for get product feedback, after getting the feedback will store on the database",
      onOk() {
        self.props.getUpdateFeedback();
        setTimeout(function() {
          notification.open({
            message: "feedback Report",
            description: "A report requested sent to MWS to get updated feedback, It will take around few minutes to get update"
          });
        }, 2000);
      },
      onCancel() {
        console.log("Cancel");
      }
    });
  };


  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  }

  _generateNegativeFeedbackDescription = numberOfNFeedback =>  numberOfNFeedback > 1 ? `You have ${numberOfNFeedback} negative feedbacks` : `You have ${numberOfNFeedback} negative feedback`

  _generateNeutralFeedbackDescription = numberOfNFeedback =>  numberOfNFeedback > 1 ? `You have ${numberOfNFeedback} neutral feedbacks` : `You have ${numberOfNFeedback} neutral feedback`

  render() {
    let { pager, data, tableData } = this.state;
    let orders;


    if (this.props.feedback && this.props.feedback.length) {
      console.log(this.props.feedback);
      data = this.props.feedback;
      tableData = this.props.feedback;
      this.state.loading = false;
    }
    const columns = [
      {
        title: "Product Info",
        dataIndex: "product_info",
        key: "product_info",
        className: "product_info",
        render: text => (<span className="product_details"><span className={"product_id"}>{text.id}</span>
                <span className={"product_title"}>{text.title}</span></span>)
      },
      {
        title: "Review Date",
        dataIndex: "date",
        key: "date",
        className: "feedback_date",
        sorter: (a, b) => {
          console.log('a', a)
          let c = new Date(a.date)
          let d = new Date(b.date)
          return d-c;
        },
        render: text => (<span className="date">{(new Date(text)).toLocaleDateString("en-US", {
          weekday: "short",
          year: "numeric",
          month: "short",
          day: "numeric"
        })}</span>)
      },
      {
        title: "Author",
        dataIndex: "author",
        key: "author",
        className: "author"
      },
      {
        title: "Rating",
        dataIndex: "rating",
        key: "rating",
        className: "rating_data",
        render: text => (
          <span className="rating_star"><Rate value={Number(text)} disabled={true} allowHalf={true}/></span>)
      },
      {
        title: "Review Details",
        dataIndex: "feedback",
        key: "feedback",
        className: "feedback_details",
        // render: text => text.map((tx, i)=> <div key={i} className="feedback_details">
        //     <span className="rating_star"><Rate value={Number(tx.rating)} disabled={true} allowHalf={true} /></span>
        //     <span className="rating_review"> - {tx.comment}</span>
        // </div>)
        render: text => (<div className="feedback_details"><span className="feedback_title"> - {text.title}</span>
          <span className="feedback_details"> - {text.details}</span></div>)
      },
      {
        title: 'Number of email sent',
        dataIndex: 'number_of_email_sent',
        key: ids.generate(),
        className: 'number_of_email_sent',
        render: () => <span>0</span>
      },

      {
        title: 'Action',
        dataIndex: 'AmazonOrderId',
        key: '_id',
        className: 'email_action',
        render: id => <Button className={id} type='primary'>Send email</Button>
      },
    ];

    return (
      <div className="card">
        <div className="card-body">
          <Row>
          {
            this.props.totalNegativeFeedback ?
            <Col span={12}>
              <div className="filter_area mr-2 my-2">
                <Alert
                  message="Negative Feedback"
                  description={this._generateNegativeFeedbackDescription(this.props.totalNegativeFeedback)}
                  type="error"
                  closable
                />
              </div>
            </Col>
            : null
          }
          {
            this.props.totalNeutralFeedback ?
            <Col span={12}>
              <div className="filter_area ml-2 my-2">
                <Alert
                  message="Neutral Feedback"
                  description={this._generateNeutralFeedbackDescription(this.props.totalNeutralFeedback)}
                  type="info"
                  closable
                />
              </div>
            </Col>
            : null
          }
        </Row>




          <div className="filter_area">
            <div className="filter_submit_btn">
              <Button type="primary" onClick={this.setFeedbackProduct}><i className="icmn-cogs mr-1"/> Generate Report
                To Get Updated Feedback</Button>
            </div>
          </div>
          <div className="filter_area d-md-flex">
              <div className="date_picker_area m-2">
                  <RangePicker
                      ranges={{ Today: [moment(), moment()], 'This Month': [moment(), moment().endOf('month')], 'Last 3 Months': [moment().subtract(3, 'months'), moment()], 'Last 6 Months': [moment().subtract(6, 'months'), moment()] , 'Last 1 Year': [moment().subtract(1, 'year'), moment()] }}
                      format="YYYY/MM/DD"
                      onChange={this.onDatePickerChange}
                  />
                  <br />
              </div>
              <div className="filter_selection m-2" style={{minWidth: 250}} >
                  <Select
                      mode="multiple"
                      size="default"
                      placeholder="Please select fields to filter"
                      onChange={this.filterItemChange}
                      style={{ width: '100%' }}>
                      {allFilters}
                  </Select>
              </div>
              <div className="filter_submit_btn m-2">
                  <Button type="primary" onClick={this.onClickFilterSearch}>Search</Button>
              </div>
          </div>
          {/*<div className="filter_area">*/}
          {/*<div className="date_picker_area">*/}
          {/*<RangePicker*/}
          {/*ranges={{ Today: [moment(), moment()], 'This Month': [moment(), moment().endOf('month')], 'Last 3 Months': [moment().subtract(3, 'months'), moment()], 'Last 6 Months': [moment().subtract(6, 'months'), moment()] , 'Last 1 Year': [moment().subtract(1, 'year'), moment()] }}*/}
          {/*format="YYYY/MM/DD"*/}
          {/*onChange={this.onDatePickerChange}*/}
          {/*/>*/}
          {/*<br />*/}
          {/*</div>*/}
          {/*<div className="filter_selection">*/}
          {/*<Select*/}
          {/*mode="multiple"*/}
          {/*size="default"*/}
          {/*placeholder="Please select fields to filter"*/}
          {/*onChange={this.filterItemChange}*/}
          {/*style={{ width: '100%' }}>*/}
          {/*{children}*/}
          {/*</Select>*/}
          {/*</div>*/}
          {/*<div className="filter_submit_btn">*/}
          {/*<Button type="primary" onClick={this.onClickFilterSearch}>Search</Button>*/}
          {/*</div>*/}
          {/*</div>*/}
        {/*</div>*/}
        {/*<div className="card-body">*/}
          <Table
            columns={columns}
            dataSource={data}
            rowKey={record => record.key}
            pagination={pager}
            onChange={this.handleTableChange}
            loading={this.state.loading}
          />
        </div>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//     return {
//         orders: state.order.orders,
//     };
// }


// export default Orders

export default Feedback;

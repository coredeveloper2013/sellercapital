import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import ids from 'short-id'
import {
  Table,
  Icon,
  Input,
  Button,
  DatePicker,
  Select,
  Radio,
  Form,
 Dropdown,
  Modal,
  notification
} from "antd";
import { connect } from "react-redux";
import { fetchTemplates, tCreationSubmit, tModifySubmit, tDeleteSubmit, sendTestMail } from "./../../../ducks/template";

// Editor Draft js
import { EditorState, convertToRaw, ContentState, Modifier } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import PropTypes from "prop-types";
import "../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import CustomOption from "./CustomOption";
import "./style.scss";
import {SERVER_STATIC_URL} from '../../../ducks/types'


const RangePicker = DatePicker.RangePicker;
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;
const confirm = Modal.confirm;


const defaultPagination = {
  pageSizeOptions: ["20", "50", "100"],
  showSizeChanger: true,
  current: 1,
  size: "small",
  showTotal: (total: number) => `Total ${total} items`,
  total: 0, pageSize: 50
};


const children = [];
let optionKeys = [{ key: "AFN", value: "AFN" }, { key: "MFN", value: "MFN" }, { key: "Shipped", value: "Shipped" },
  { key: "Pending", value: "Pending" }, { key: "Unshipped", value: "Unshipped" },
  { key: "Delivered", value: "Delivered" }, { key: "Canceled", value: "Canceled" }, {
    key: "Returned",
    value: "Returned"
  }];
for (let i = 0; i < optionKeys.length; i++) {
  children.push(<Option key={optionKeys[i].key}>{optionKeys[i].value}</Option>);
}


const mapStateToProps = (state, props) => ({
  templates: state.template.templates
});


const mapDispatchToProps = (dispatch, props) => ({
  fetchTemplates: dispatch(fetchTemplates()),
  templateCreateSubmit: values => dispatch(tCreationSubmit(values)),
  templateModifySubmit: values => dispatch(tModifySubmit(values)),
  deleteModifySubmit: id => dispatch(tDeleteSubmit(id)),
  sendTestMail: templateID => dispatch(sendTestMail(templateID))
});


@connect(mapStateToProps, mapDispatchToProps)


class TemplateComponents extends React.Component {

  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: "",
    emailTemplate: '',
    filtered: false,
    loading: true,
    createTemplateVisible: false,
    confirmLoadingCTemplate: false,
    editorState: EditorState.createEmpty(),
    modifyTemplate: "",
    deteteTemplateId: "",
    showTemplatePreview: false,
    templateBody: "",
    templateTitle: "",
    templateID: "",
    redirectToCTemplate: false,
    userEmail: '',
    emailError: false,
    filter_items: [],
  };

  showCreateTemplate = () => {
    const { form } = this.props;
    this.setState({
      createTemplateVisible: true,
      modifyTemplate: "",
      editorState: EditorState.createEmpty()
    });
    form.setFieldsValue({ name: "", subject: "", style: "", type: "" });
  };

  setRedirectToCTemplate = () => {
    this.setState({
      redirectToCTemplate: true
    });
  };

  handleCTemplateOk = () => {
    this.setState({
      confirmLoadingCTemplate: true
    });
    setTimeout(() => {
      this.setState({
        createTemplateVisible: false,
        confirmLoadingCTemplate: false,
        modifyTemplate: ""
      });
    }, 1000);
  };


  handleCTemplateCancel = () => {
    this.setState({
      createTemplateVisible: false,
      showTemplatePreview: false
    });
  };

  validateEmail = (email)  => {
    const re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    return re.test(email);
  }

  sendTestMail = (id) => {
    const {userEmail}  = this.state
    let payload = {
      "id": id,
      "email": userEmail
    }
    console.log('payload', payload)
    if (this.validateEmail(userEmail)) {
      this.setState({emailError: false})
      this.props.sendTestMail(payload)
      this.handleCTemplateCancel()
    } else {
      this.setState({emailError: true})
    }
  }

  _generateEmailTemplate = (templateData) => {
    var logo = "https://reviewkick.s3.amazonaws.com/uploads/ckeditor/pictures/15/content_amazon-logo.png";
    if(templateData.logo){
      logo = SERVER_STATIC_URL + templateData.logo;
    }
    var email_attachment = '';
    if(templateData.email_attachment){

      var attachment = SERVER_STATIC_URL + templateData.email_attachment;
      var attachment_name = templateData.email_attachment.split('/')[2];
      email_attachment = `There is an attachment with this email. Click this link to view the attachment.
                            <a href="${attachment}" target="_blank">${attachment_name}</a>
                        `;
    }
    let htmlBody = `
          <table bgcolor="#f2f2f2" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
              <tr>
                <td align="center" style="padding: 30px 34%;">
                  <img class="fr-fic" src="${logo}" style="display: block; height: 77.3333px; margin: 5px auto; vertical-align: top; width: 160px;">
                </td>
              </tr>
              <tr>
                <td class="template_area" style="padding: 5px;">
                  ${templateData.email_message}
                </td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
              <tr>
                <td style="padding: 15px 5px;">${email_attachment}</td>
              </tr>
            </tbody>
          </table>
      `;
    return htmlBody;
  }


  previewTemplate = (id) => {
    const templateData = this.props.templates.filter((temp) => temp["_id"] === id)[0];
    const emailTemplate = this._generateEmailTemplate(templateData)

    this.setState({
      templateBody: templateData.email_message,
      templateTitle: templateData.template_name,
      templateID: templateData["_id"],
      emailTemplate: emailTemplate,
    });
    this.setState({
      showTemplatePreview: true
    });
  };

  deleteTemplate = (id) => {
    this.setState({
      deteteTemplateId: id
    });
    let self = this;
    confirm({
      title: "Do you Want to delete the template?",
      content: "You are about to delete template, please make sure !",
      okText: "Yes",
      onOk() {
        self.props.deleteModifySubmit(id);
      },
      onCancel() {

      }
    });
  };


  submitCreateTemplate = (e) => {
    e.preventDefault();
    const { form, dispatch } = this.props;
    const { modifyTemplate } = this.state;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.body = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
        if (!modifyTemplate) {
          this.props.templateCreateSubmit(values);
        } else {
          values.id = modifyTemplate;
          this.props.templateModifySubmit(values);
        }
        this.handleCTemplateOk();
      }
    });
  };

  onDatePickerChange = (date, dateString) => {
    if (dateString && dateString.length) {
      this.setState({ date_start: dateString[0], date_end: dateString[1] });
    }
  };

  filterItemChange = values => {
    this.setState({ filter_items: values });
  };


  handleSizeChange = (e) => {
    this.setState({ size: e.target.value });
  };

  onInputChange = (e) => {
    this.setState({ searchText: e.target.value });
  };

  editTemplate = (id) => {
    this.setState({ modifyTemplate: id });
  };


  editTemplateModal = (id) => {
    this.setState({ modifyTemplate: id });
    // const { form } = this.props
    // const templateData = this.props.templates.filter((temp) => temp['_id'] === id)[0];
    // this.setState({ modifyTemplate: id });
    // if(typeof templateData === 'object' && Object.keys(templateData).length){
    //     const {name, subject, style, type } = templateData;
    //     form.setFieldsValue({name, subject, style, type });
    //     const contentBlock = htmlToDraft(templateData.body);
    //     const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
    //     const editorState = EditorState.createWithContent(contentState);
    //     this.setState({
    //         editorState
    //     });
    // }
    // this.setState({
    //     createTemplateVisible: true,
    // });
  };

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState
    });
  };

  renderRedirectToCreateTemplate = () => {
    if (this.state.redirectToCTemplate) {
      return <Redirect to='/templates/create'/>;
    }
  };

  onSearch = () => {
    const { searchText, tableData } = this.state;
    let reg = new RegExp(searchText, "gi");
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: tableData
        .map(record => {
          let match = record.name.match(reg);
          if (!match) {
            return null;
          }
          return {
            ...record,
            name: (
              <span>
                {record.name
                  .split(reg)
                  .map(
                    (text, i) =>
                      i > 0 ? [<span className="highlight">{match[0]}</span>, text] : text
                  )}
              </span>
            )
          };
        })
        .filter(record => !!record)
    });
  };

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };
  filterItemChange = values => {
    this.setState({ filter_items: values });
  }

  render() {
    console.log('this.props', this.props)
    let {
      pager,
      data,
      tableData,
      createTemplateVisible,
      confirmLoadingCTemplate,
      editorState,
      showTemplatePreview,
      templateTitle,
      templateBody,
      templateID,
      modifyTemplate,
      emailTemplate,
       } = this.state;
    let templates;

    if (modifyTemplate) {
      return <Redirect to={"/templates/edit/" + modifyTemplate}/>;
    }

    let { getFieldDecorator } = this.props.form;
    const formHalfArea = {
      labelCol: { span: 4 },
      wrapperCol: { span: 14 }
    };


    if (this.props.templates && this.props.templates.length) {
      data = this.props.templates;
      tableData = this.props.templates;
      this.state.loading = false;
    }
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const columns = [
      {
        title: "Template Name",
        dataIndex: "template_name",
        key: ids.generate()
      },
      {
        title: "Email Subject",
        dataIndex: "email_subject",
        key: ids.generate()

      },
      {
        title: "Type",
        dataIndex: "template_type",
        key: ids.generate()
      },
      {
        title: "Status",
        dataIndex: "template_status",
        key: ids.generate()
      },
      {
        title: 'Number of email sent',
        dataIndex: 'number_of_email_sent',
        key: ids.generate(),
        className: 'number_of_email_sent',
        render: () => <span>0</span>
      },
      {
        title: "Action",
        key:  ids.generate(),
        dataIndex: "_id",
        render: (id) => (
          <span>
            <a href={"/#/templates/edit/" + id} className="mr-2">
              <i className="icmn-pencil mr-1"/> Edit
            </a>&nbsp;
            <a href="javascript: void(0);" onClick={() => this.previewTemplate(id)}>
              <i className="icmn-enlarge mr-1"/> Preview
            </a> &nbsp;
            <a href="javascript: void(0);" onClick={() => this.deleteTemplate(id)}>
              <i className="icmn-cross mr-1"/> Remove
            </a>
          </span>
        )
      }
    ];

    return (

      <div className="card">
        {this.renderRedirectToCreateTemplate()}
        <div className="card-header">
          <div className="create-template">
            <Button type="primary" onClick={this.setRedirectToCTemplate}>
              Create Template
            </Button>
            <div>
              <Modal title="Create A New Template"
                     visible={createTemplateVisible}
                     onOk={this.submitCreateTemplate}
                     className={"centered"}
                     maskClosable={false}
                     confirmLoading={confirmLoadingCTemplate}
                     onCancel={this.handleCTemplateCancel} width="790px">

                <Form layout="horizontal" onSubmit={this.submitCreateTemplate} className="create_template">
                  <FormItem>
                    <label className="form-label mb-0">Template Name:</label>
                    {getFieldDecorator("name", {
                      initialValue: "",
                      rules: [{ required: false }]
                    })(<Input placeholder="Template name"/>)}
                  </FormItem>

                  <FormItem>
                    <label className="form-label mb-0">Email Subject:</label>
                    {getFieldDecorator("subject", {
                      initialValue: "",
                      rules: [{ required: false }]
                    })(<Input placeholder="Email Subject"/>)}
                  </FormItem>

                  <FormItem>
                    <label className="form-label mb-0">Type:</label>
                    {getFieldDecorator("type", {
                      initialValue: "",
                      rules: [{ required: false }]
                    })(<Input placeholder="Email type"/>)}
                  </FormItem>

                  <FormItem>
                    <label className="form-label mb-0">Style</label>
                    {getFieldDecorator("style", {
                      initialValue: "",
                      rules: [{ required: false }]
                    })(<Input placeholder="Style"/>)}
                  </FormItem>

                  <FormItem>
                    <label className="form-label mb-0">Body</label>

                    {getFieldDecorator("body", {
                      initialValue: "",
                      rules: [{ required: false }]
                    })(<Editor
                      editorState={editorState}
                      wrapperClassName="demo-wrapper"
                      editorClassName="demo-editor"
                      onEditorStateChange={this.onEditorStateChange}
                      toolbarCustomButtons={[<CustomOption/>]}
                    />)}

                  </FormItem>

                </Form>
              </Modal>
            </div>
            <div>
              <Modal title={templateTitle}
                     visible={showTemplatePreview}
                // onOk={this.submitCreateTemplate}
                     className={"centered"}
                     maskClosable={false}
                     onCancel={this.handleCTemplateCancel} width="790px"
                     footer={[
                        <div key="hello world" className='d-flex justify-content-between align-items-start'>
                          <div className="d-inline-block">
                            <Input
                              onChange={(e) => this.setState({userEmail: e.target.value})}
                              className="d-inline mr-2"
                              placeholder="Enter your email address" />
                            {
                              this.state.emailError ?
                                <label className='text-danger'>Please add a valid email address</label> : null
                            }
                          </div>
                          <div>
                            <Button key='sendTestMail' icon='mail' onClick={() => this.sendTestMail(templateID)}>Send Test
                              Mail</Button>
                            <Button key='backToTemplate' onClick={this.handleCTemplateCancel}>Return</Button>
                          </div>
                        </div>
                     ]}>

                <div dangerouslySetInnerHTML={{ __html: emailTemplate }}></div>

              </Modal>
            </div>
          </div>
        </div>
        <div className="card-body">
          <Table
            columns={columns}
            dataSource={data}
            rowKey={record => record.key}
            pagination={pager}
            onChange={this.handleTableChange}
            loading={this.state.loading}
          />
        </div>
      </div>
    );
  }
}

// export default templates
const Template = Form.create()(TemplateComponents);
export default Template;

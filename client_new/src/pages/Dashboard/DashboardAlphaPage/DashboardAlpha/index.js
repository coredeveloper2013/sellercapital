import React from 'react'
import {
  Button,
  Input,
  Icon,
  Table,
  DatePicker,
  Spin,
  Card,
  message,
} from 'antd'

import moment from 'moment'
import StarRatingComponent from 'react-star-rating-component';
import connect from "react-redux/es/connect/connect";
import PaymentCard from 'components/CleanComponents/PaymentCard'
import PaymentAccount from 'components/CleanComponents/PaymentAccount'
import PaymentTx from 'components/CleanComponents/PaymentTx'
import ChartCard from 'components/CleanComponents/ChartCard'
import { tableData } from './data.json'
import { getUser, resendWelcomeMail } from './../../../../ducks/dashboard'
import {fetchFeedbackAnalysis} from '../../../../ducks/feedback'
import {fetchOrderAnalysis} from '../../../../ducks/order'
import './style.scss'
import dummy from './dummydata.js'
import DatePickerMonthSelection from '../../../pageComponents/DatePickerMonthSelection'
import DashboardFeedbackChart from '../../../pageComponents/DashboardFeedbackChart'
import DashboardOrderChart from '../../../pageComponents/DashboardOrderChart'

const RangePicker = DatePicker.RangePicker;


const mapStateToProps = (state, props) => ({
  feedbackAnalysis: state.feedback.feedbackAnalysis,
  orderAnalysis: state.order.orderAnalysis,
  feedbackAnalysisLoader: state.feedback.feedbackAnalysisLoader,
  orderAnalysisLoader: state.order.orderAnalysisLoader,
})

const mapDispatchToProps = (dispatch, props) => ({
    user: getUser(),
    resendWelcomeMail : () => {
        dispatch(resendWelcomeMail())
    },
    fetchFeedbackAnalysis: (param) =>  dispatch(fetchFeedbackAnalysis(param)),
    fetchOrderAnalysis: (param) => dispatch(fetchOrderAnalysis(param)),
})

@connect(mapStateToProps, mapDispatchToProps)

class DashboardAlpha extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          isUser: false,
          feedbackAnalysisDate: [],
          orderAnalysisDate: [],
          mode: ['month', 'month'],

        };
    }

    componentDidMount = () => {
      let {
        fetchFeedbackAnalysis,
        fetchOrderAnalysis,
      }  = this.props
      fetchFeedbackAnalysis()
      fetchOrderAnalysis()
    }

    sendWelcomeMailAgain = () =>{
        this.props.resendWelcomeMail();
    }

    ConfirmEmailMessage = (props) => {
        const user = props.isUser;
        if(!user.confirmEmail){
            return (
                <div className="confirm_mail_notification align-items-center" >
                    <h3 className="align-content-center">Hi there, welcome to Saller Capital</h3>
                    <p>Let's get started</p>
                    <p>We have sent you a welcome email to <a href="mailto:email@customer.com"></a> with an activation link to verify your email address on file with us. </p>
                    <p>Please check your inbox and click on the active account link or button.</p>
                    <p>If you don't see the email in your inbox, please also try looking in the spam folder</p>
                    <a className="btn btn-primary" href="#/profile">Edit Account Profile</a> &nbsp; <span className="btn btn-primary" onClick={this.sendWelcomeMailAgain}>Resend Welcome Email</span>
                </div>
            );
        }
        return ('');
    }

  generateChartCard = (title, amount) => {
    return <ChartCard
      title={title}
      amount={amount}
      chartProps={{
        width: 120,
        height: 107,
        lines: [
          {
            values: [2, 11, 8, 14, 18, 20, 26],
            colors: {
              area: 'rgba(199, 228, 255, 0.5)',
              line: '#004585',
            },
          },
        ],
      }}
    />
  }
  render() {
    let {
      feedbackAnalysis,
      orderAnalysis,
      feedbackAnalysisLoader,
      orderAnalysisLoader,
    } = this.props
    let {user} = this.state;

      if (this.props.user) {
          user = this.props.user;
      }

    const tableColumns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Position',
        dataIndex: 'position',
        key: 'position',
      },
      {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
        sorter: (a, b) => a.age - b.age,
      },
      {
        title: 'Office',
        dataIndex: 'office',
        key: 'office',
      },
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
      },
      {
        title: 'Salary',
        dataIndex: 'salary',
        key: 'salary',
        sorter: (a, b) => a.salary - b.salary,
      },
    ]


    return (
      <div>
        <this.ConfirmEmailMessage isUser={user} />

        <div className="row">
        {
          dummy.topLinks.map((link, i) =>
            <div key={i} className="col-xl-3">
              {this.generateChartCard(link.title, link.amount)}
            </div>
            )
        }
        </div>

        <div className="row">
          <div className="col-lg-6 my-3">
            <Card
              extra={<DatePickerMonthSelection apiFunc={this.props.fetchOrderAnalysis} />}
             title="Orders" >
               <DashboardOrderChart
                orderAnalysis={orderAnalysis}
                orderAnalysisLoader={orderAnalysisLoader}
                />
            </Card>
          </div>
          <div className="col-lg-6 my-3">
            <Card
            extra={<DatePickerMonthSelection apiFunc={this.props.fetchFeedbackAnalysis} />}
            title="Feedback" >
              <DashboardFeedbackChart
                feedbackAnalysis={feedbackAnalysis}
                feedbackAnalysisLoader={feedbackAnalysisLoader}
               />
            </Card>
          </div>

        </div>

        <div className="row">
          <div className="col-md-6">
            <div className="card">
              <div className="card-header">
                <h2>Most Recent Feedback</h2>
              </div>
              <div className="card-body">
                {
                  dummy.recentFeedbacks.map((feedback, i) =>
                    <div key={i} className="card">
                      <div className="card-body">
                        <h3>{feedback.comment}</h3>
                        <h4>
                           <StarRatingComponent
                            name={feedback.comment}
                            starCount={5}
                            value={feedback.rating}
                          />
                        </h4>
                        <p>product: <a href={feedback.link}>{feedback.product_title}</a></p>
                        <p>user: {feedback.user.name}</p>
                      </div>
                    <hr/>
                    </div>

                    )
                }
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="card">
              <div className="card-header">
                <h2>Top ten products</h2>
              </div>
              <div className="card-body">
                <Table
                  columns={dummy.topTenProductsColumns}
                  dataSource={dummy.topTenProducts}
                  pagination={false}
                />

              </div>
            </div>
          </div>
        </div>



{
  // old code following
}

        <div className="utils__title utils__title--flat mb-3">
          <span className="text-uppercase font-size-16">Last Week Statistics</span>
        </div>
        <div className="row">
          <div className="col-xl-4">
            <ChartCard
              title={'Transactions'}
              amount={'1240'}
              chartProps={{
                width: 120,
                height: 107,
                lines: [
                  {
                    values: [2, 11, 8, 14, 18, 20, 26],
                    colors: {
                      area: 'rgba(199, 228, 255, 0.5)',
                      line: '#004585',
                    },
                  },
                ],
              }}
            />
          </div>
          <div className="col-xl-4">
            <ChartCard
              title={'Income'}
              amount={'$1,240.00'}
              chartProps={{
                width: 120,
                height: 107,
                lines: [
                  {
                    values: [20, 80, 67, 120, 132, 66, 97],
                    colors: {
                      area: 'rgba(199, 228, 255, 0.5)',
                      line: '#004585',
                    },
                  },
                ],
              }}
            />
          </div>
          <div className="col-xl-4">
            <ChartCard
              title={'Outcome'}
              amount={'$240.56'}
              chartProps={{
                width: 120,
                height: 107,
                lines: [
                  {
                    values: [42, 40, 80, 67, 84, 20, 97],
                    colors: {
                      area: 'rgba(199, 228, 255, 0.5)',
                      line: '#004585',
                    },
                  },
                ],
              }}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-header">
                <div className="utils__title">Recently Referrals</div>
                <div className="utils__titleDescription">
                  Block with important Recently Referrals information
                </div>
              </div>
              <div className="card-body">
                <Table
                  columns={tableColumns}
                  dataSource={tableData}
                  pagination={false}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="utils__title utils__title--flat mb-3">
          <span className="text-uppercase font-size-16">Your Cards (3)</span>
          <Button className="ml-3">View All</Button>
        </div>
        <div className="row">
          <div className="col-lg-4">
            <PaymentCard
              icon={'lnr lnr-bookmark'}
              name={'Matt Daemon'}
              number={'4512-XXXX-1678-7528'}
              type={'VISA'}
              footer={'Expires at 02/20'}
              sum={'$2,156.78'}
            />
          </div>
          <div className="col-lg-4">
            <PaymentCard
              icon={'lnr lnr-bookmark'}
              name={'David Beckham'}
              number={'8748-XXXX-1678-5416'}
              type={'MASTERCARD'}
              footer={'Expires at 03/22'}
              sum={'$560,245.35'}
            />
          </div>
          <div className="col-lg-4">
            <PaymentCard
              icon={'lnr lnr-hourglass'}
              name={'Mrs. Angelina Jolie'}
              number={'6546-XXXX-1678-1579'}
              type={'VISA'}
              footer={'Locked Temporary'}
              sum={'$1,467,98'}
            />
          </div>
        </div>
        <div className="utils__title utils__title--flat mb-3">
          <span className="text-uppercase font-size-16">Your Accounts (6)</span>
          <Button className="ml-3">View All</Button>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <PaymentAccount
              icon={'lnr lnr-inbox'}
              number={'US 4658-1678-7528'}
              footer={'Current month charged: $10,200.00'}
              sum={'$2,156.78'}
            />
          </div>
          <div className="col-lg-6">
            <PaymentAccount
              icon={'lnr lnr-inbox'}
              number={'IBAN 445646-8748-4664-1678-5416'}
              footer={'Current month charged: $1,276.00'}
              sum={'$560,245.35'}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <PaymentAccount
              icon={'lnr lnr-inbox'}
              number={'US 4658-1678-7528'}
              footer={'Current month charged: $10,200.00'}
              sum={'$2,156.78'}
            />
          </div>
          <div className="col-lg-6">
            <PaymentAccount
              icon={'lnr lnr-inbox'}
              number={'IBAN 445646-8748-4664-1678-5416'}
              footer={'Current month charged: $1,276.00'}
              sum={'$560,245.35'}
            />
          </div>
        </div>
        <div className="utils__title mb-3">
          <span className="text-uppercase font-size-16">Recent Transactions (167)</span>
          <Button className="ml-3">View All</Button>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <PaymentTx
              income={false}
              amount={'-$100.00'}
              info={'US 4658-1678-7528'}
              footer={'To AMAZON COPR, NY, 1756'}
            />
            <PaymentTx
              income={true}
              amount={'+27,080.00'}
              info={'4512-XXXX-1678-7528'}
              footer={'To DigitalOcean Cloud Hosting, Winnetka, LA'}
            />
            <PaymentTx
              income={false}
              amount={'-100,000.00'}
              info={'6245-XXXX-1678-3256'}
              footer={'To Tesla Cars, LA, USA'}
            />
            <div className="text-center pb-5">
              <Button type="primary" className="width-200" loading>
                Load More...
              </Button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DashboardAlpha
